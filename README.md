# PDF2Image

โปรแกรมแปลง PDF เป็นไฟล์ Image โดยแบ่งแต่ละหน้าเป็นไฟล์รูปภาพ

## วิธีใช้งาน
1. กดปุ่ม Select File เพื่อเลือกไฟล์ PDF 
2. กดปุ่ม Start เพื่อเริ่มการทำงาน
3. จะขึ้นหน้าต่างให้เลือกที่เก็บไฟล์ (ควรสร้างโฟลเดอร์ไว้เก็บไฟล์)
4. รอจนโปรแกรมขึ้น popup เสร็จสิ้น

## File
1. [Source Code ](https://gitlab.com/Chettato/pdf2image/-/blob/main/PDF2Image.py)
2. [Dowloand](https://gitlab.com/Chettato/pdf2image/-/raw/main/PDF2Image.exe?inline=false)

