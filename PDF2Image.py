from pdf2image import convert_from_path
import os
import tkinter as Tk
from tkinter import *
from tkinter import filedialog
from tkinter import messagebox
import tkinter.scrolledtext as scrolledtext
from tkinter import ttk
def Scan(ina,Out):
    file = ina
    output = Out
    images = convert_from_path(file)

   

    for i in range(len(images)): 
        # Save pages as images in the pdf
        images[i].save(Out+'page'+ str(i) +'.jpg', 'JPEG')
    print("Success")
    
def SelectPDF():
    global ina,cheSA
    
    cheSA=0
    BB = filedialog.askopenfilename(defaultextension='.pdf', filetypes=[('PDF Files', '*.pdf')])
    if BB == '':
        messagebox.showwarning("Information",'ที่อยู่ไฟล์ไม่ถูกต้อง')
    else:
        messagebox.showinfo("Information",'สำเร็จ')
        ina=BB
        cheSA=1
    la.insert(END,ina)
    la.config(state=DISABLED)
def SelectSaveAs():
    global Out,cheSA
    
    cheSA=0
    AA = filedialog.askdirectory(mustexist=True)
    if AA == '':
        messagebox.showwarning("Information",'ที่อยู่ไฟล์ไม่ถูกต้อง')
    else:
        messagebox.showinfo("Information",'สำเร็จ')
        Out=AA
        cheSA=1
    Scan(ina,Out)
    messagebox.showinfo("Information","เสร็จสิ้น")

window=Tk()
window.resizable(False, False)
#*---------------------------------------------------------------------------- #
#*#LINK                          Main USER INTERFACE Space                     #
#* --------------------------------------------------------------------------- #
v0=IntVar()
aw=IntVar()
v1 = IntVar()
v2 = IntVar()
v3 = IntVar()
v4 = IntVar()
rft=IntVar()
rft2=IntVar()
v0.set(0)
In=Button(window,text='Select File',command=SelectPDF)
Out=Button(window,text='Start',command=SelectSaveAs)
la=Entry(window,width=70)
la.place(x=50,y=5)
In.place(x=50,y=30)
Out.place(x=50,y=60)
window.title('PDF2Image')
window.geometry("500x100+10+10")
window.mainloop()